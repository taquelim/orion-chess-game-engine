﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using ChessGameEngine.ClassLibrary;

namespace ChessGameEngine.UI
{
    /*
    * PIECE=WHITE/black
    * pawn=P/p
    * kinght (horse)=N/n
    * bishop=B/b
    * rook (castle)=R/r
    * Queen=Q/q
    * King=K/k
    */

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            string[,] chessBoard = BoardGeneration.InitiateStandardChess();

            StartPiecesOnTable(chessBoard);
        }

        /// <summary>
        /// Asigns the chessboard array to the grid
        /// </summary>
        private void StartPiecesOnTable(string[,] chessboard)
        {
            //if (chessboard[0, 0].Equals("b"))
            //    a8_image.Source = "/ChessPieces/black_bishop.png";


        }


    }
}
