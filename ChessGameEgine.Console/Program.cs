﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessGameEngine.ClassLibrary;

namespace ChessGameEgine.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] chessboard = BoardGeneration.InitiateStandardChess();

            for (int i = 0; i < 64; i++)
            {
                if (i % 8 == 0)
                    System.Console.Write("\n");

                System.Console.Write(chessboard[i / 8, i % 8] + " ");
            }

            //Moves.PossibleMoves("", );

            System.Console.ReadLine();

        }
    }
}
