﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameEngine.ClassLibrary
{
    public class Moves
    {
        static long FILE_A = 72340172838076673L;
        static long FILE_H = -9187201950435737472L;
        static long FILE_AB = 217020518514230019L;
        static long FILE_GH = -4557430888798830400L;
        static long RANK_1 = -72057594037927936L;
        static long RANK_4 = 1095216660480L;
        static long RANK_5 = 4278190080L;
        static long RANK_8 = 255L;
        static long CENTRE = 103481868288L;
        static long EXTENDED_CENTRE = 66229406269440L;
        static long KING_SIDE = -1085102592571150096L;
        static long QUEEN_SIDE = 1085102592571150095L;
        static long KING_B7 = 460039L;
        static long KNIGHT_C6 = 43234889994L;
        static long NOT_WHITE_PIECES; // black pieces expect the black king, pieces that can be captured
        static long BLACK_PIECES; // expect the black king, for legal moves purposes
        static long EMPTY;


        public static string PossibleMovesW(string history, long WP, long WN, long WB, long WR,
            long WQ, long WK, long BP, long BN, long BB, long BR, long BQ, long BK)
        {
            // added BK to avoid illegal capture
            NOT_WHITE_PIECES = ~(WP | WN | WB | WR | WQ | WK | BK);
            // omitted BK to avoid illegal capture
            BLACK_PIECES = BP | BN | BB | BR | BQ;
            EMPTY = ~(WP | WN | WB | WR | WQ | WK | BP | BN | BB | BR | BQ | BK);

            TimeExperiment(WP);
            string list = PossiblePW(history, WP);

            return list;
        }

        /// <summary>
        /// Calculates the possible posibilities for the White Pawn
        /// </summary>
        /// <param name="history"></param>
        /// <param name="WP"></param>
        /// <returns></returns>
        public static string PossiblePW(string history, long WP)
        {
            string list = "";

            //x1,y1,x2,y2
            //capture right
            long PAWN_MOVES = (WP >> 7) & BLACK_PIECES & ~RANK_8 & ~FILE_A;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i / 8 + 1) + (i % 8 - 1) + (i / 8) + (i % 8);
                }
            }

            //capture left
            PAWN_MOVES = (WP >> 9) & BLACK_PIECES & ~RANK_8 & ~FILE_H;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i / 8 + 1) + (i % 8 + 1) + (i / 8) + (i % 8);
                }
            }
            //move 1 forward
            PAWN_MOVES = (WP >> 8) & EMPTY & ~RANK_8;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i / 8 + 1) + (i % 8) + (i / 8) + (i % 8);
                }
            }
            PAWN_MOVES = (WP >> 16) & EMPTY & (EMPTY >> 8) & RANK_4;//move 2 forward
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i / 8 + 2) + (i % 8) + (i / 8) + (i % 8);
                }
            }

            //y1,y2,Promotion Type,"P"
            //pawn promotion by capture right
            PAWN_MOVES = (WP >> 7) & BLACK_PIECES & RANK_8 & ~FILE_A;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i % 8 - 1) + (i % 8) + "QP" + (i % 8 - 1) + (i % 8) + 
                        "RP" + (i % 8 - 1) + (i % 8) + "BP" + (i % 8 - 1) + (i % 8) + "NP";
                }
            }

            //pawn promotion by capture left
            PAWN_MOVES = (WP >> 9) & BLACK_PIECES & RANK_8 & ~FILE_H;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i % 8 + 1) + (i % 8) + "QP" + (i % 8 + 1) + (i % 8) + 
                        "RP" + (i % 8 + 1) + (i % 8) + "BP" + (i % 8 + 1) + (i % 8) + "NP";
                }
            }

            //pawn promotion by move 1 forward
            PAWN_MOVES = (WP >> 8) & EMPTY & RANK_8;
            for (int i = BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i < 64 - BitwiseOperations.NumberOfTrailingZeros(PAWN_MOVES); i++)
            {
                if (((PAWN_MOVES >> i) & 1) == 1)
                {
                    list += "" + (i % 8) + (i % 8) + "QP" + (i % 8) + (i % 8) + "RP" + 
                        (i % 8) + (i % 8) + "BP" + (i % 8) + (i % 8) + "NP";
                }
            }

            //y1,y2,Space,"E
            return list;
        }
        public static void TimeExperiment(long WP)
        {

        }

        public static void TEMethodA(int loopLength, long WP)
        {

        }

        public static void TEMethodB(int loopLength, long WP)
        {

        }

    }
}
