﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameEngine.ClassLibrary
{
    /*
    * PIECE=WHITE/black
    * pawn=P/p
    * kinght (horse)=N/n
    * bishop=B/b
    * rook (castle)=R/r
    * Queen=Q/q
    * King=K/k
    */
    public class BoardGeneration
    {
        /// <summary>
        /// Initiates a standard game of chess, returning the result of the chess board
        /// </summary>
        /// <returns> Array of the chessboard </returns>
        public static string[,] InitiateStandardChess()
        {
            // Initiate all the 12 Bitboards
            // Where the starting W is white and B is black, and the following is the 
            // starting letter of the piece, P is pawn.
            long WP = 0L, WN = 0L, WB = 0L, WR = 0L, WQ = 0L, WK = 0L, BP = 0L, BN = 0L, BB = 0L, BR = 0L, BQ = 0L, BK = 0L;

            string[,] chessBoard = new string[,]{
                {"r","n","b","q","k","b","n","r"},
                {"p","p","p","p","p","p","p","p"},
                {" "," "," "," "," "," "," "," "},
                {" "," "," "," "," "," "," "," "},
                {" "," "," "," "," "," "," "," "},
                {" "," "," "," "," "," "," "," "},
                {"P","P","P","P","P","P","P","P"},
                {"R","N","B","Q","K","B","N","R"}
            };

            return ArrayToBitboards(chessBoard, WP, WN, WB, WR, WQ, WK, BP, BN, BB, BR, BQ, BK);
        }

        /// <summary>
        /// Initiliazes a version of chess, chess960
        /// </summary>
        public static string[,] InitiateChess960()
        {
            long WP = 0L, WN = 0L, WB = 0L, WR = 0L, WQ = 0L, WK = 0L, BP = 0L, BN = 0L, BB = 0L, BR = 0L, BQ = 0L, BK = 0L;

            string[,] chessBoard={
            {" "," "," "," "," "," "," "," "},
            {"p","p","p","p","p","p","p","p"},
            {" "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "},
            {" "," "," "," "," "," "," "," "},
            {"P","P","P","P","P","P","P","P"},
            {" "," "," "," "," "," "," "," "}};

            // Steps of how to setup the board of a chess960 game
            // following the guide in chess960 wikipedia website

            // Step 1 - Place a bishop on a random black square :
            int random1 =(int)(Helper.RandomDouble() * 8);
            chessBoard[0, random1] = "b";
            chessBoard[7, random1] = "B";

            // Step 2 - Place a bishop on the white square indicated:
            int random2 = (int)(Helper.RandomDouble() * 8);
            while (random2 % 2 == random1 % 2)
            {
                random2 = (int)(Helper.RandomDouble() * 8);
            }
            chessBoard[0, random2] = "b";
            chessBoard[7, random2] = "B";

            // Step 3 - Place the Queen on the first empty position, allways skiping filed positions:
            int random3 = (int)(Helper.RandomDouble() * 8);
            while (random3 == random1 || random3 == random2)
            {
                random3 = (int)(Helper.RandomDouble() * 8);
            }
            chessBoard[0, random3] = "q";
            chessBoard[7, random3] = "Q";

            // Step 4 - Place a knight on the empty position indicated:
            int random4a = (int)(Helper.RandomDouble() * 5);
            int counter = 0;
            int loop = 0;
            while (counter - 1 < random4a)
            {
                if (" ".Equals(chessBoard[0, loop])) { counter++; }
                loop++;
            }
            chessBoard[0, loop - 1] = "n";
            chessBoard[7, loop - 1] = "N";
            int random4b = (int)(Helper.RandomDouble() * 4);
            counter = 0;
            loop = 0;
            while (counter - 1 < random4b)
            {
                if (" ".Equals(chessBoard[0, loop])) { counter++; }
                loop++;
            }
            chessBoard[0, loop - 1] = "n";
            chessBoard[7, loop - 1] = "N";

            // Step 5 - Place the other knight on the empty position indicated:
            counter = 0;
            while (!" ".Equals(chessBoard[0, counter]))
            {
                counter++;
            }
            chessBoard[0, counter] = "r";
            chessBoard[7, counter] = "R";
            while (!" ".Equals(chessBoard[0, counter]))
            {
                counter++;
            }
            chessBoard[0, counter] = "k";
            chessBoard[7, counter] = "K";
            while (!" ".Equals(chessBoard[0, counter]))
            {
                counter++;
            }
            chessBoard[0, counter] = "r";
            chessBoard[7, counter] = "R";


            return ArrayToBitboards(chessBoard, WP, WN, WB, WR, WQ, WK, BP, BN, BB, BR, BQ, BK);
        }

        /// <summary>
        /// Transforms the array of the starting chess board to a bitboard.
        /// </summary>
        /// <param name="chessboard"></param>
        /// <param name="WP">White Pawn</param>
        /// <param name="WN">White Knight</param>
        /// <param name="WB">White Bishop</param>
        /// <param name="WR">White Rouge</param>
        /// <param name="WQ">White Queen</param>
        /// <param name="WK">White King</param>
        /// <param name="BP">Black Pawn</param>
        /// <param name="BN">Black Knight</param>
        /// <param name="BB">Black Bishop</param>
        /// <param name="BR">Black Rougue</param>
        /// <param name="BQ">Black Queen</param>
        /// <param name="BK">Black Knight</param>
        /// <returns></returns>
        public static string[,] ArrayToBitboards(string[,] chessboard, long WP, long WN, long WB,
            long WR, long WQ, long WK, long BP, long BN, long BB, long BR, long BQ, long BK)
        {
            string binary;

            for (int i = 0; i < 64; i++)
            {
                binary = "0000000000000000000000000000000000000000000000000000000000000000";
                binary = binary.Substring(i + 1) + "1" + binary.Substring(0, i);

                switch (chessboard[i / 8, i % 8])
                {
                    case "P":
                        WP += ConvertStringToBitboard(binary);
                        break;
                    case "N":
                        WN += ConvertStringToBitboard(binary);
                        break;
                    case "B":
                        WB += ConvertStringToBitboard(binary);
                        break;
                    case "R":
                        WR += ConvertStringToBitboard(binary);
                        break;
                    case "Q":
                        WQ += ConvertStringToBitboard(binary);
                        break;
                    case "K":
                        WK += ConvertStringToBitboard(binary);
                        break;
                    case "p":
                        BP += ConvertStringToBitboard(binary);
                        break;
                    case "n":
                        BN += ConvertStringToBitboard(binary);
                        break;
                    case "b":
                        BB += ConvertStringToBitboard(binary);
                        break;
                    case "r":
                        BR += ConvertStringToBitboard(binary);
                        break;
                    case "q":
                        BQ += ConvertStringToBitboard(binary);
                        break;
                    case "k":
                        BK += ConvertStringToBitboard(binary);
                        break;
                }
            }

            return DrawArray(WP, WN, WB, WR, WQ, WK, BP, BN, BB, BR, BQ, BK);
        }

        /// <summary>
        /// Converts the binary strings to binary numbers (long)
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        public static long ConvertStringToBitboard(string binary)
        {
            if (binary[0] == '0') 
            {
                //not going to be a negative number
                return Convert.ToInt64(binary, 2);
            }
            else
            {
                return Convert.ToInt64("1" + binary.Substring(2), 2) * 2;
            }
        }

        /// <summary>
        /// Creates and fills an array with the bitboards, for debugging purposes
        /// </summary>
        /// <param name="WP">White Pawn</param>
        /// <param name="WN">White Knight</param>
        /// <param name="WB">White Bishop</param>
        /// <param name="WR">White Rouge</param>
        /// <param name="WQ">White Queen</param>
        /// <param name="WK">White King</param>
        /// <param name="BP">Black Pawn</param>
        /// <param name="BN">Black Knight</param>
        /// <param name="BB">Black Bishop</param>
        /// <param name="BR">Black Rougue</param>
        /// <param name="BQ">Black Queen</param>
        /// <param name="BK">Black Knight</param>
        private static string[,] DrawArray(long WP, long WN, long WB, long WR, long WQ, long WK, long BP, long BN, long BB, long BR, long BQ, long BK)
        {
            string[,] chessboard = new string[8, 8];

            for (int i = 0; i < 64; i++)
            {
                chessboard[i / 8, i % 8] = " ";
            }

            for (int i = 0; i < 64; i++) {
                if (((WP >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "P"; }
                if (((WN >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "N"; }
                if (((WB >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "B"; }
                if (((WR >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "R"; }
                if (((WQ >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "Q"; }
                if (((WK >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "K"; }
                if (((BP >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "p"; }
                if (((BN >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "n"; }
                if (((BB >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "b"; }
                if (((BR >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "r"; }
                if (((BQ >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "q"; }
                if (((BK >> i) & 1) == 1) { chessboard[i / 8, i % 8] = "k"; }
            }

            return chessboard;

        }
    }
}
