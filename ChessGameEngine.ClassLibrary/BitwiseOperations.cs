﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameEngine.ClassLibrary
{
    /// <summary>
    /// Class for bitwise operations for debuging or 
    /// calculation purposes
    /// </summary>
    public static class BitwiseOperations
    {
        public static string ToBinary(this int value)
        {
            if (value < 2) return value.ToString();

            var divisor = value / 2;
            var remainder = value % 2;

            return (ToBinary(divisor) + remainder);
        }

        public static string ToNumber(this string value)
        {
            return Convert.ToInt64(value, 2).ToString();
        }

        public static int NumberOfTrailingZeros(long n)
        {
            int mask = 1;
            for (int i = 0; i < 64; i++, mask <<= 1)
                if ((n & mask) != 0)
                    return i;

            return 64;
        }
    }
}
