﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ChessGameEngine.ClassLibrary
{
    public static class Helper
    {
        private static Random random = new Random();

        /// <summary>
        /// Return a random double between 0 and 1.
        /// </summary>
        /// <returns></returns>
        public static double RandomDouble()
        {
            double randomNumber = random.NextDouble();

            return randomNumber;
        }
    }
}
